# capacitor-wifi-plugin

Capacitor plugin for get wifi info and connect to wifi network

## Install

```bash
npm install capacitor-wifi-plugin
npx cap sync
```

## API

<docgen-index>

* [`checkPermissions()`](#checkpermissions)
* [`requestPermissions()`](#requestpermissions)
* [`isWiFiEnabled()`](#iswifienabled)
* [`isLocationEnabled()`](#islocationenabled)
* [`getWiFiInfo()`](#getwifiinfo)
* [`scan()`](#scan)
* [`connectToWiFi(...)`](#connecttowifi)
* [Type Aliases](#type-aliases)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### checkPermissions()

```typescript
checkPermissions() => Promise<{ location: LocationState; }>
```

**Returns:** <code>Promise&lt;{ location: <a href="#locationstate">LocationState</a>; }&gt;</code>

--------------------


### requestPermissions()

```typescript
requestPermissions() => Promise<{ location: LocationState; }>
```

**Returns:** <code>Promise&lt;{ location: <a href="#locationstate">LocationState</a>; }&gt;</code>

--------------------


### isWiFiEnabled()

```typescript
isWiFiEnabled() => Promise<{ enabled: boolean; }>
```

**Returns:** <code>Promise&lt;{ enabled: boolean; }&gt;</code>

--------------------


### isLocationEnabled()

```typescript
isLocationEnabled() => Promise<{ enabled: boolean; }>
```

**Returns:** <code>Promise&lt;{ enabled: boolean; }&gt;</code>

--------------------


### getWiFiInfo()

```typescript
getWiFiInfo() => Promise<WiFiInfo>
```

**Returns:** <code>Promise&lt;<a href="#wifiinfo">WiFiInfo</a>&gt;</code>

--------------------


### scan()

```typescript
scan() => Promise<{ networks: any[]; }>
```

**Returns:** <code>Promise&lt;{ networks: any[]; }&gt;</code>

--------------------


### connectToWiFi(...)

```typescript
connectToWiFi(options: WiFiOptions) => Promise<WiFiInfo>
```

| Param         | Type                                                |
| ------------- | --------------------------------------------------- |
| **`options`** | <code><a href="#wifioptions">WiFiOptions</a></code> |

**Returns:** <code>Promise&lt;<a href="#wifiinfo">WiFiInfo</a>&gt;</code>

--------------------


### Type Aliases


#### LocationState

<code>'prompt' | 'denied' | 'granted'</code>


#### WiFiInfo

<code>{ ssid: string; bssid?: string; rssi?: number; }</code>


#### WiFiOptions

<code>{ ssid: string; password?: string; }</code>

</docgen-api>
