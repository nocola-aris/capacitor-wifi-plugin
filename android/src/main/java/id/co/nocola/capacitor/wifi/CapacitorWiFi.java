package id.co.nocola.capacitor.wifi;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiNetworkSpecifier;
import android.net.wifi.WifiNetworkSuggestion;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class CapacitorWiFi {
    public final String TAG = "CapacitorWiFi";
    private Context context;
    private WifiManager wifiManager;
    private BroadcastReceiver wifiScanReceiver;
    private ConnectivityManager connectivityManager;
    private ConnectivityManager.NetworkCallback networkCallback;

    public CapacitorWiFi(Context context) {
        this.context = context;
        this.wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        this.connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public boolean isWiFiEnabled() {
        return wifiManager.isWifiEnabled();
    }

    public boolean isLocationEnabled() {
        int mode;

        try {
            mode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            mode = 0;
        }

        return mode > 0;
    }

    public WiFiInfo getWiFiInfo() {
        return new WiFiInfo(wifiManager.getConnectionInfo());
    }

    @SuppressLint("MissingPermission")
    public void scanWiFi(WiFiScanResolver resolver) {
        try {
            wifiScanReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context c, Intent intent) {
                    boolean success = intent.getBooleanExtra(WifiManager.EXTRA_RESULTS_UPDATED, false);
                    List<ScanResult> results = wifiManager.getScanResults();
                    resolver.resolve(results, !success);
                    context.unregisterReceiver(wifiScanReceiver);
                }
            };

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
            context.registerReceiver(wifiScanReceiver, intentFilter);

            boolean success = wifiManager.startScan();

            if (!success) {
                List<ScanResult> results = wifiManager.getScanResults();
                resolver.resolve(results, false);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void connectToWiFi(String ssid, String password, String algorithm, boolean isHidden, WiFiConnectionResolver resolver) {
        Log.d(TAG, "SSID: " + ssid);
        Log.d(TAG, "Password: " + password);
        Log.d(TAG, "Algorithm: " + algorithm);
        Log.d(TAG, "Hidden Network: " + isHidden);

        if (Build.VERSION.SDK_INT >= 29) {
            try {
                WifiNetworkSpecifier.Builder specifierBuilder = new WifiNetworkSpecifier.Builder();
                specifierBuilder.setSsid(ssid);

                if (algorithm != null) {
                    Log.d(TAG, "WEP/WPA/WPA2: " + algorithm.matches("/WEP|WPA|WPA2/gim"));
                    Log.d(TAG, "WPA3: " + algorithm.matches("/WPA3/gim"));

                    if (algorithm.matches("/WEP|WPA|WPA2/gim") && !password.isBlank()) {
                        specifierBuilder.setWpa2Passphrase(password);
                    }

                    if (algorithm.matches("/WPA3/gim") && !password.isBlank()) {
                        specifierBuilder.setWpa3Passphrase(password);
                    }
                }

                if (isHidden) {
                    specifierBuilder.setIsHiddenSsid(true);
                }

                WifiNetworkSpecifier specifier = specifierBuilder.build();

                NetworkRequest networkRequest = new NetworkRequest.Builder()
                        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
//                        .removeCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                        .setNetworkSpecifier(specifier)
                        .build();

                connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                networkCallback = new ConnectivityManager.NetworkCallback() {
                    @Override
                    public void onAvailable(Network network) {
                        super.onAvailable(network);
                        resolver.resolve(network);
                        connectivityManager.bindProcessToNetwork(network);
                    }

                    @Override
                    public void onLost(@NonNull Network network) {
                        super.onLost(network);
                        connectivityManager.unregisterNetworkCallback(networkCallback);
                    }

                    @Override
                    public void onUnavailable() {
                        super.onUnavailable();
                        resolver.reject("SPECIFIER_NETWORK_UNAVAILABLE");
                        connectivityManager.unregisterNetworkCallback(networkCallback);
                    }
                };

                connectivityManager.requestNetwork(networkRequest, networkCallback);
            } catch (Exception e) {
                resolver.reject(e.getMessage());
            }
        } else {
            resolver.reject("REQUIRE_ANDROID_29_UP");
        }
    }
}
