package id.co.nocola.capacitor.wifi;

import android.Manifest;
import android.net.Network;
import android.net.wifi.ScanResult;
import android.os.Build;
import android.util.Log;

import com.getcapacitor.JSArray;
import com.getcapacitor.JSObject;
import com.getcapacitor.PermissionState;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;
import com.getcapacitor.annotation.Permission;
import com.getcapacitor.annotation.PermissionCallback;

import java.util.List;

@CapacitorPlugin(
        name = "CapacitorWiFi",
        permissions = {
                @Permission(
                        alias = "wifi-33-up",
                        strings = {
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.NEARBY_WIFI_DEVICES,
                                Manifest.permission.ACCESS_WIFI_STATE,
                                Manifest.permission.CHANGE_WIFI_STATE,
                                Manifest.permission.ACCESS_NETWORK_STATE,
                                Manifest.permission.CHANGE_NETWORK_STATE
                        }
                ),
                @Permission(
                        alias = "wifi-32-down",
                        strings = {
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_WIFI_STATE,
                                Manifest.permission.CHANGE_WIFI_STATE,
                                Manifest.permission.ACCESS_NETWORK_STATE,
                                Manifest.permission.CHANGE_NETWORK_STATE
                        }
                )
        }
)
public class CapacitorWiFiPlugin extends Plugin {
    public final String TAG = "CapacitorWiFiPlugin";

    private String permissionAlias;

    private CapacitorWiFi implementation;


    @Override
    public void load() {
        super.load();

        permissionAlias = Build.VERSION.SDK_INT >= 33 ? "wifi-33-up" : "wifi-32-down";
        Log.d(TAG, "Permission alias: " + permissionAlias);

        implementation = new CapacitorWiFi(getContext());
    }

    @PluginMethod()
    public void checkPermissions(PluginCall call) {
        Log.d(TAG, "Permission state: " + getPermissionState(permissionAlias));
        JSObject result = new JSObject();

        if (getPermissionState(permissionAlias) == PermissionState.GRANTED) {
            result.put("location", "granted");
        } else if (getPermissionState(permissionAlias) == PermissionState.DENIED) {
            result.put("location", "denied");
        } else {
            result.put("location", "prompt");
        }

        call.resolve(result);
    }

    @PluginMethod()
    public void requestPermissions(PluginCall call) {
        if (getPermissionState(permissionAlias) == PermissionState.GRANTED || getPermissionState(permissionAlias) == PermissionState.DENIED) {
            checkPermissions(call);
            return;
        }

        Log.d(TAG, "Requesting permission...");
        requestPermissionForAlias(permissionAlias, call, "handlePermissionCallback");
    }

    @PermissionCallback
    private void handlePermissionCallback(PluginCall call) {
        if (call != null) {
            return;
        }

        if (call.getMethodName().equals("requestPermissions")) {
            checkPermissions(call);
            return;
        }

        if (getPermissionState(permissionAlias) != PermissionState.GRANTED) {
            return;
        }

        switch (call.getMethodName()) {
            case "isWiFiEnabled":
                isWiFiEnabled(call);
                break;
            case "isLocationEnabled":
                isLocationEnabled(call);
                break;
            case "getWiFiInfo":
                getWiFiInfo(call);
                break;
            case "scan":
                scan(call);
                break;
            case "connectToWiFi":
                connectToWiFi(call);
                break;
        }
    }

    @PluginMethod()
    public void isWiFiEnabled(PluginCall call) {
        if (getPermissionState(permissionAlias) != PermissionState.GRANTED) {
            requestPermissionForAlias(permissionAlias, call, "handlePermissionCallback");
            return;
        }

        JSObject result = new JSObject();

        result.put("enabled", implementation.isWiFiEnabled());
        Log.d(TAG, "isWiFiEnabled: " + result.getString("enabled"));

        call.resolve(result);
    }

    @PluginMethod()
    public void isLocationEnabled(PluginCall call) {
        if (getPermissionState(permissionAlias) != PermissionState.GRANTED) {
            requestPermissionForAlias(permissionAlias, call, "handlePermissionCallback");
            return;
        }

        JSObject result = new JSObject();

        result.put("enabled", implementation.isLocationEnabled());
        Log.d(TAG, "isLocationEnabled: " + result.getString("enabled"));

        call.resolve(result);
    }

    @PluginMethod()
    public void getWiFiInfo(PluginCall call) {
        if (getPermissionState(permissionAlias) != PermissionState.GRANTED) {
            requestPermissionForAlias(permissionAlias, call, "handlePermissionCallback");
            return;
        }

        WiFiInfo info = implementation.getWiFiInfo();

        JSObject result = CapacitorWiFiPluginHelper.WiFiInfoToJSObject(info);
        Log.d(TAG, "WiFiInfo: " + result);

        call.resolve(result);
    }

    @PluginMethod()
    public void scan(PluginCall call) {
        if (getPermissionState(permissionAlias) != PermissionState.GRANTED) {
            requestPermissionForAlias(permissionAlias, call, "handlePermissionCallback");
            return;
        }

        implementation.scanWiFi(new WiFiScanResolver() {
            @Override
            public void resolve(List<ScanResult> scanResults, boolean isOld) {
                JSObject result = new JSObject();
                JSArray networks = CapacitorWiFiPluginHelper.scanResultsToJSArray(scanResults);
                result.put("networks", networks);
                Log.d(TAG, "networks: " + result);
                call.resolve(result);
            }
        });
    }

    @PluginMethod()
    public void connectToWiFi(PluginCall call) {
        if (getPermissionState(permissionAlias) != PermissionState.GRANTED) {
            requestPermissionForAlias(permissionAlias, call, "handlePermissionCallback");
            return;
        }

        String ssid = call.getString("ssid");
        String password = call.getString("password");
        String algorithm = call.getString("algorithm");
        boolean isHidden = Boolean.TRUE.equals(call.getBoolean("isHidden"));

        Log.d(TAG, "Connecting to WiFi network");

        if (ssid == null || ssid.isBlank()) {
            call.reject("SSID_REQUIRED");
            return;
        }

        if (password == null) {
            password = "";
        }


        if (algorithm == null && !password.isBlank()) {
            algorithm = "WPA";
        }

        implementation.connectToWiFi(ssid, password, algorithm, isHidden, new WiFiConnectionResolver() {
            @Override
            public void resolve(Network network) {
                getWiFiInfo(call);
            }

            @Override
            public void reject(String message) {
                Log.e(TAG, "Connecting to WiFi network failed: " + message);
                call.reject(message);
            }
        });
    }
}
