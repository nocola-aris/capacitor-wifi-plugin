package id.co.nocola.capacitor.wifi;

import android.net.wifi.ScanResult;

import com.getcapacitor.JSArray;
import com.getcapacitor.JSObject;

import java.util.List;

public class CapacitorWiFiPluginHelper {
    public static JSObject scanResultToJSObject(ScanResult scanResult) {
        JSObject result = new JSObject();
        result.put("ssid", scanResult.SSID);
        result.put("bssid", scanResult.BSSID);
        result.put("frequency", scanResult.frequency);
        result.put("timestamp", scanResult.timestamp);
        return result;
    }
    public static JSArray scanResultsToJSArray(List<ScanResult> scanResults) {
        JSArray result = new JSArray();
        for (ScanResult scanResult: scanResults) {
            result.put(scanResultToJSObject(scanResult));
        }
        return result;
    }
    public static JSObject WiFiInfoToJSObject(WiFiInfo info) {
        JSObject result = new JSObject();
        result.put("ssid", info.getSSID());
        result.put("bssid", info.getBSSID());
        result.put("rssi", info.getRSSI());
        return result;
    }
}
