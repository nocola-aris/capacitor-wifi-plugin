package id.co.nocola.capacitor.wifi;

import android.net.Network;

public interface WiFiConnectionResolver {
    public void resolve(Network network);
    public void reject(String message);
}
