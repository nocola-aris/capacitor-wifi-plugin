package id.co.nocola.capacitor.wifi;

import android.net.wifi.WifiInfo;

public class WiFiInfo {
    private String ssid;
    private String bssid;
    private double rssi;
    WiFiInfo(WifiInfo info) {
        this.ssid = info.getSSID();
        this.bssid = info.getBSSID();
        this.rssi = info.getRssi();
    }
    public String getSSID() {
        return ssid;
    }
    public String getBSSID() {
        return bssid;
    }
    public double getRSSI() {
        return rssi;
    }
}
