package id.co.nocola.capacitor.wifi;

import android.net.wifi.ScanResult;

import java.util.List;

public interface WiFiScanResolver {
    public void resolve(List<ScanResult> result, boolean isOld);
}
