import Foundation
import NetworkExtension
import SystemConfiguration
import SystemConfiguration.CaptiveNetwork
import CoreLocation

@objc public class CapacitorWiFi: NSObject {
    var locationManager: CLLocationManager?
    
    public override init() {
        locationManager = CLLocationManager()
    }
    
    @objc public func isWiFiEnabled() async -> Bool {
        return (await getWiFiInfo()) != nil
    }
    
    @objc public func getLocationManager() -> CLLocationManager? {
        return locationManager
    }
    
    @objc public func getLocationState() -> String {
        let locationState: String

        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationState = "prompt"
        case .restricted, .denied:
            locationState = "denied"
        case .authorizedAlways, .authorizedWhenInUse:
            locationState = "granted"
        @unknown default:
            locationState = "prompt"
        }
        
        return locationState
    }
    
    @objc public func isLocationEnabled() -> Bool {
        return CLLocationManager.locationServicesEnabled()
    }
    
    @objc public func getWiFiInfo() async -> [String: Any]? {
        if #available(iOS 14.0, *) {
            if let wifiInfo = await NEHotspotNetwork.fetchCurrent() {
                return [
                    "ssid": wifiInfo.ssid,
                    "bssid": wifiInfo.bssid,
                    "rssi": wifiInfo.signalStrength
                ]
            }
        } else {
            if let interfaceNames = CNCopySupportedInterfaces() as? [String] {
                for interfaceName in interfaceNames {
                    if let interfaceInfo = CNCopyCurrentNetworkInfo(interfaceName as CFString) as NSDictionary? {
                        return [
                            "ssid": interfaceInfo[kCNNetworkInfoKeySSID as String] as Any,
                            "bssid": interfaceInfo[kCNNetworkInfoKeyBSSID as String] as Any
                        ]
                    }
                }
            }
        }
        
        return nil
    }
    
    
    @objc func connectToWiFi(ssid: String, password: String?, completionHandler: ((Error?) -> Void)? = nil) {
        let configuration = password != nil ? NEHotspotConfiguration(ssid: ssid, passphrase: password!, isWEP: false) : NEHotspotConfiguration(ssid: ssid)
        NEHotspotConfigurationManager.shared.apply(configuration, completionHandler: completionHandler)
    }
}
