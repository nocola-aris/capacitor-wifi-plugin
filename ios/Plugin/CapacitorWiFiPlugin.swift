import Foundation
import Capacitor
import CoreLocation

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(CapacitorWiFiPlugin)
public class CapacitorWiFiPlugin: CAPPlugin {
    var permissionCallID: String?
    private var service = CapacitorWiFi()
    
    @objc public override func checkPermissions(_ call: CAPPluginCall) {
        let locationState = service.getLocationState()
        call.resolve(["location": locationState])
    }

    @objc public override func requestPermissions(_ call: CAPPluginCall) {
        if let manager = service.getLocationManager(), service.isLocationEnabled() {
            if CLLocationManager.authorizationStatus() == .notDetermined {
                bridge?.saveCall(call)
                permissionCallID = call.callbackId
                manager.requestWhenInUseAuthorization()
            } else {
                checkPermissions(call)
            }
        } else {
            call.reject("Location services are disabled")
        }
    }

    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if let callID = permissionCallID, let call = bridge?.savedCall(withID: callID) {
            checkPermissions(call)
            bridge?.releaseCall(call)
        }
    }
    
    @objc func isWiFiEnabled(_ call: CAPPluginCall) {
        Task.init {
            let enabled = await service.isWiFiEnabled()
            call.resolve(["enabled": enabled])
        }
    }
    
    @objc func isLocationEnabled(_ call: CAPPluginCall) {
        let enabled = service.isLocationEnabled()
        call.resolve(["enabled": enabled])
    }
    
    @objc func getWiFiInfo(_ call: CAPPluginCall) {
        Task.init {
            if let wiFiInfo = await service.getWiFiInfo() {
                call.resolve(wiFiInfo)
                return
            }
            
            call.reject("WIFI_DISABLED")
        }
    }
    
    @objc func connectToWiFi(_ call: CAPPluginCall) {
        if let ssid = call.getString("ssid") {
            service.connectToWiFi(ssid: ssid, password: call.getString("password")) { error in
                if let error = error {
                    call.reject(error.localizedDescription, nil, error)
                } else {
                    self.getWiFiInfo(call)
                }
            }
            
            return
        }
        
        call.reject("SSID_EMPTY")
    }
}
