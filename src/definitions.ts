export interface CapacitorWiFiPlugin {
  checkPermissions(): Promise<{ location: LocationState }>;
  requestPermissions(): Promise<{ location: LocationState }>;
  isWiFiEnabled(): Promise<{ enabled: boolean }>;
  isLocationEnabled(): Promise<{ enabled: boolean }>;
  getWiFiInfo(): Promise<WiFiInfo>;
  scan(): Promise<{ networks: any[] }>;
  connectToWiFi(options: WiFiOptions): Promise<WiFiInfo>;
}

export type LocationState = 'prompt' | 'denied' | 'granted';

export type WiFiOptions = {
  ssid: string;
  password?: string;
}

export type WiFiInfo = {
  ssid: string;
  bssid?: string;
  rssi?: number;
}