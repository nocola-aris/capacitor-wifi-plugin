import { registerPlugin } from '@capacitor/core';

import type { CapacitorWiFiPlugin } from './definitions';

const CapacitorWiFi = registerPlugin<CapacitorWiFiPlugin>('CapacitorWiFi', {
  web: () => import('./web').then(m => new m.CapacitorWiFiWeb()),
});

export * from './definitions';
export { CapacitorWiFi };
