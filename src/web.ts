import { WebPlugin } from '@capacitor/core';

import type { CapacitorWiFiPlugin, LocationState, WiFiInfo } from './definitions';

export class CapacitorWiFiWeb extends WebPlugin implements CapacitorWiFiPlugin {
  checkPermissions(): Promise<{ location: LocationState; }> {
    throw this.unavailable('This method is not available in web.');
  }
  requestPermissions(): Promise<any> {
    throw this.unavailable('This method is not available in web.');
  }
  isWiFiEnabled(): Promise<{ enabled: boolean; }> {
    throw this.unavailable('This method is not available in web.');
  }
  isLocationEnabled(): Promise<{ enabled: boolean; }> {
    throw this.unavailable('This method is not available in web.');
  }
  getWiFiInfo(): Promise<WiFiInfo> {
    throw this.unavailable('This method is not available in web.');
  }
  scan(): Promise<{ networks: any[]; }> {
    throw this.unavailable('This method is not available in web.');
  }
  connectToWiFi(): Promise<WiFiInfo> {
    throw this.unavailable('This method is not available in web.');
  }
}
